import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12 // без него не будет работать механизм layout
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.12 // для 2 лабы: воспроизведение и запись видео
import QtQuick.Dialogs 1.2 // для "открыть видео" во 2 лабе, пока не реализовано
import QtQuick.Controls.Styles 1.4 // можно менять стили кнопки, но в итоге пока не используется
import QtGraphicalEffects 1.0 // графические эффекты для 3 лабы

Page { // лабараторная 2: запись и воспроизведение видео
    id: page_lab2  
    header: ToolBar{
        
        id: header_lab2
        Material.background: "#000000"
        
        Rectangle {
            
            id: screen2
            height: parent.height
            width: parent.width
            property int pixelSize: screen2.height * 1.1
            property color textColor: "#d9d9d9" //светло-серый
            
            
            //height: 40
            color: "#000000"
            border.color: "#282828"
            Row {
                width: 300
                height: 20
                
                Text {
                    id: text2;
                    font.pixelSize: 17;
                    font.family: "lucida sans unicode";
                    anchors.left: parent.left;
                    anchors.leftMargin: 50
                    color: screen2.textColor;
                    text: qsTr("Запись и воспроизведение");
                    anchors.top: parent.top
                    anchors.horizontalCenter: screen2.verticalCenter
                }
            }
            Row {
                width: 300
                height: 20
                Text{
                    font.pixelSize: 17;
                    font.family: "lucida sans unicode";
                    anchors.left: parent.left;
                    anchors.leftMargin: 64
                    color: screen2.textColor;
                    text: qsTr("мультимедиа-файлов.")
                    anchors.top: parent.top
                    anchors.topMargin: 22
                    anchors.horizontalCenter: parent.horizontalCenter
                }
            }
        }
        Image{ // иконка в заголовке
            source: "bird.JPG"
            width: 35
            height: 40
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            //  anchors.leftMargin: 5
        }
        
        Image{ // правая картинка в менюшке - звездочки
            source: "zvr.JPG"
            width: 20
            height: 25
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            //  anchors.leftMargin: 5
        }
    } // конец заголовка
    
    ColumnLayout {
        anchors.fill:parent
        RowLayout{
            
            RadioButton {
                id: video
                checked: true
                text: qsTr("Видео")
            }
            RadioButton {
                id: camera
                text: qsTr("Камера")
            }
        }
        RowLayout{
            Item{ // страница с видео-проигрывателем
                id: page_v
                width: parent.width
                height: parent.height
                ColumnLayout{
                    width: parent.width
                    height: parent.height
                    
                    RowLayout{
                        ColumnLayout{
                            Image {
                                source: "music.png"
                                height: 10
                                width: 10
                            }
                        }
                        ColumnLayout{
                            Slider{
                                id: slid_volum
                                from: 0
                                to: 1
                                value: 0.3
                            }
                        }
                        
                    }
                    RowLayout{
                        id: video_controller
                        ColumnLayout{
                            RoundButton{
                                Image {
                                    id: back
                                    source: "back.png"
                                }
                                flat: true
                                onClicked: robot_video.seek(robot_video.position-2000)
                            }
                        }
                        ColumnLayout{
                            RoundButton{
                                Image {
                                    id: stop
                                    source: "stop.png"
                                }
                                flat: true
                                onClicked: robot_video.stop()
                            }
                        }
                        ColumnLayout{
                            RoundButton{
                                Image {
                                    id: play_pause
                                    source: {
                                        if (robot_video.playbackState == MediaPlayer.PlayingState)
                                            return "pause.png"
                                        else
                                            return "play.png"
                                    }
                                    
                                }
                                // Material.background: Material.color("#000000", Shade50)
                                flat: true
                                
                                onClicked: {
                                    if (robot_video.playbackState == MediaPlayer.PlayingState)
                                        return robot_video.pause();
                                    else
                                        return robot_video.play();
                                }
                            }
                        }
                        ColumnLayout{
                            RoundButton{
                                Image {
                                    id: next
                                    source: "next.png"
                                }
                                flat: true
                                onClicked: robot_video.seek(robot_video.position+2000)
                            }
                        }
                    }
                    
                    RowLayout{
                        
                        MediaPlayer{
                            id: robot_video
                            source: "anime.mp4"
                            //loops: 6
                            volume: slid_volum.value
                        }
                    }
                    RowLayout{
                        Slider{
                            id: slid_time
                            from: 0
                            to: robot_video.duration
                            value: robot_video.position
                            onPressedChanged: {
                                robot_video.seek(slid_time.value)
                            }
                        }
                        anchors.bottom: outv.bottom
                        
                        
                    }
                    
                    VideoOutput {
                        id: outv
                        anchors.fill: parent
                        source: robot_video
                        anchors.top: video_controller.bottom
                    }
                    
                    visible: {
                        if(video.checked == true){
                            return true
                        }
                        else
                            return false
                    }
                }
            }
            
            Item{ // страница с камерой
                id: page_c
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: camera.bottom
                anchors.bottom: parent.bottom
                anchors.verticalCenter: parent.verticalCenter
                
                Camera{
                    id: foto_video
                    imageCapture{
                        onImageCaptured: {
                            photoPreview.source = preview
                        }
                    }
                }
                
                VideoOutput{
                    id: photocam
                    source: foto_video
                    anchors.left: page_c.left
                    anchors.right: page_c.right
                    anchors.top: camera.bottom
                    anchors.bottom: page_c.bottom
                    anchors.leftMargin: 10
                    anchors.rightMargin: 10
                    anchors.bottomMargin: 10
                    anchors.verticalCenter: parent.verticalCenter
                    
                    /* Image {
                        id: photoPreview
                        height: 40
                        width: 75
                        anchors.right: parent.right
                        
                        MouseArea {
                            anchors.fill: parent;
                            onClicked: photoPreview.width = 355, photoPreview.height = 190
                            onDoubleClicked: photoPreview.width = 75, photoPreview.height = 40
                        }
                    }*/
                }
                RowLayout{
                    id: button_fv
                    spacing: 20
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: photocam.bottom
                    anchors.bottomMargin: 50
                    
                    RoundButton{ // сделать фото или поставить видео на паузу
                        id: foto_button
                        Image {
                            source: {
                                if (foto_video.videoRecorder.recorderState == CameraRecorder.StoppedState)
                                    return "foto.png"
                                else if (foto_video.videoRecorder.recorderStatus == CameraRecorder.RecordingStatus)
                                    return "video_pause.png"
                                else if (foto_video.videoRecorder.recorderStatus == CameraRecorder.PausedStatus)
                                    return "video_start.png"
                            }
                        }
                        flat: true
                        onClicked: {
                            if (foto_video.videoRecorder.recorderState == CameraRecorder.StoppedState){
                                foto_video.imageCapture.captureToLocation("C:/Kavallini_181_331/Kavallini_181_331_mobile/foto") // метод захвата неподвижного объекта (фото)
                                // или можно foto_video.imageCapture.capture() - тогда фото сохранится в место по умолчанию
                            }
                            else if (foto_video.videoRecorder.recorderStatus == CameraRecorder.RecordingStatus)
                                foto_video.videoRecorder.recorderStatus = CameraRecorder.PausedStatus
                            else if (foto_video.videoRecorder.recorderStatus == CameraRecorder.PausedStatus)
                                foto_video.videoRecorder.recorderStatus = CameraRecorder.RecordingStatus
                        }
                    }
                    
                    RoundButton { // начать видео или остановить его
                        id: video_button
                        Image {
                            source: {
                                if (foto_video.videoRecorder.recorderStatus == CameraRecorder.RecordingStatus)
                                    return "video_stop.png"
                                else
                                    return "video.png"
                            }
                        }
                        flat: true
                        onClicked: {
                            if(foto_video.videoRecorder.recorderState == CameraRecorder.StoppedState){
                                foto_video.videoRecorder.outputLocation = "C:/Kavallini_181_331/Kavallini_181_331_mobile/video";
                                foto_video.videoRecorder.record() // начать запись
                            }
                            else
                                foto_video.videoRecorder.stop() // остановить
                        }
                    }
                }
                visible: {
                    if(video.checked == false){
                        return true
                    }
                    else
                        return false
                }
            }
            
        }
    }
    
}
