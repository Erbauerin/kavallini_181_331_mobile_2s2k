import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12 // без него не будет работать механизм layout
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.12 // для 2 лабы: воспроизведение и запись видео
import QtQuick.Dialogs 1.2 // для "открыть видео" во 2 лабе, пока не реализовано
import QtQuick.Controls.Styles 1.4 // можно менять стили кнопки, но в итоге пока не используется
import QtGraphicalEffects 1.0 // графические эффекты для 3 лабы
import QtQuick.Window 2.12
import QtQml 2.12
import QtWebView 1.1
import QtWebSockets 1.1


Page{ // лабораторная 6: REST API запросы с Яндекс.Диска
    id: page_lab6
    header: ToolBar{

        id: header_lab6
        Material.background: "#000000"

        Rectangle {
            id: screen6
            height: 30
            width: parent.width
            property int pixelSize: screen6.height
            property color textColor: "#d9d9d9" //светло-серый

            color: "#000000"
            border.color: "#282828"
            Row {
                width: 300
                height: 10

                Text {
                    id: text6;
                    width: screen6.width
                    horizontalAlignment: Text.AlignHCenter

                    font.pixelSize: 17;
                    font.family: "lucida sans unicode";

                    color: screen6.textColor;
                    text: "REST API"
                    anchors.top: parent.top
                   // anchors.verticalCenter: screen3.width.horisontalCenter
                }
            }
        }
    }

    GridView{
         model: fileModel // модель, из которой берутся данные
         id: gridrest
         visible: false
         anchors.top: parent.top
         anchors.left: parent.left
         anchors.right: parent.right
         anchors.bottom: rowforrest.top
         anchors.bottomMargin: 120
         anchors.topMargin: 30
         anchors.leftMargin: 15
         anchors.horizontalCenter: parent.horizontalCenter
         cellWidth: 180
         cellHeight: 180

         delegate: Column { // делегат - это отдельный компонент, нужен для того, чтобы "создать макет" для вывода информации в модели
             // проблема делегата в том, что он настолько отдельный, что у него нет родителя, для "зависимости" нужно использовать id
             Image {
                 source: preview
                 sourceSize.width: 150
                 sourceSize.height: 150
             }

             Label{
                 text: name
                 font.pixelSize: 17
             }

             Label{
                 text: size + " байт"
                 font.pixelSize: 13
                 font.family: "lucida sans unicode"
             }

             Label{
                 text: "Дата зарузки: " + created
                 font.pixelSize: 13
                 font.family: "lucida sans unicode"
             }
         }
     }

     Label{
         id: noaccesslab6
         font.pixelSize: 16
         font.family: "lucida sans unicode"
         anchors.horizontalCenter: gridrest.horizontalCenter
         lineHeight: 1.5
         text: "Пожалуйста, разрешите приложению\nправа на чтение Яндекс Диска,\nдля дальнейшего получения\nактуального списка фото."
         visible: if (fileModel.rowCount() > 0)
                      false
                  else true
     }

     RowLayout{
         id: rowforrest
         spacing: 50
         anchors.bottom: parent.bottom
         anchors.bottomMargin: 15
         height: 40
         Material.background: "#000000"
         anchors.horizontalCenter: parent.horizontalCenter

         Button {
             id: btnforreload
             flat: true
             text: "Обновить"
             height: 150
             width: 150
             onClicked: if (token1 !== 0) {
                            listrest.visible = true
                            gridrest.visible = false
                            noaccesslab6.visible = false
                            Http_controller.requestReceivingAPI(token1)
                        }
                        else {
                            noaccesslab6.visible = true
                            listrest.visible = false
                            gridrest.visible = false
                        }
         }

        }


}
