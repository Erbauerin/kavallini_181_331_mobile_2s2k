import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12 // без него не будет работать механизм layout
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.12 // для 2 лабы: воспроизведение и запись видео
import QtQuick.Dialogs 1.2 // для "открыть видео" во 2 лабе, пока не реализовано
import QtQuick.Controls.Styles 1.4 // можно менять стили кнопки, но в итоге пока не используется
import QtGraphicalEffects 1.0 // графические эффекты для 3 лабы
import QtQuick.Window 2.12
import QtQml 2.12
import QtWebView 1.1
import QtWebSockets 1.1


Page{ // лабараторная 10: мессенджер
    id: page_lab10
    
    header: ToolBar{

        id: header_lab10
        Material.background: "#000000"

        Rectangle {
            id: screen10
            height: 30
            width: parent.width
            property int pixelSize: screen10.height
            property color textColor: "#d9d9d9" //светло-серый

            color: "#000000"
            border.color: "#282828"
            Row {
                width: 300
                height: 10

                Text {
                    width: screen10.width
                    horizontalAlignment: Text.AlignHCenter

                    font.pixelSize: 17;
                    font.family: "lucida sans unicode";

                    color: screen10.textColor;
                    text: "~Чат-Бот~"
                    anchors.top: parent.top
                }
            }
        }
    }

    ListModel{
            id: modellab10
        }

        Rectangle{
            color: "#000000"
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: rectanlesend.top
            width: parent.width
            height: parent.height*0.85

            ListView{
                anchors.fill: parent
                anchors.bottomMargin: 70
                anchors.topMargin: 10
                spacing: 4
                model: modellab10
                delegate:

                    Item{
                    width: parent.width
                    height: borderimagelab10.height

                    BorderImage {
                        id: borderimagelab10
                        source: outmessage ? "send.png" : "receive.gif"
                        width: parent.width *0.8
                        height: textmessage.contentHeight + 25
                        anchors.left: outmessage ? undefined : parent.left
                        anchors.right: outmessage ? parent.right : undefined

                        Text {
                            id: textmessage
                            color: "black"
                            text: message
                            width: borderimagelab10.width*0.8
                            font.pointSize: 12
                            font.family: "lucida sans unicode"
                            anchors.top: borderimagelab10.top
                            anchors.left: borderimagelab10.left
                            anchors.leftMargin: 6
                            anchors.topMargin: 6
                            wrapMode: TextArea.WrapAtWordBoundaryOrAnywhere
                        }

                        Text {
                            id: textfortimelab10
                            color: "#cef26b"
                            text: Qt.formatDateTime(new Date(), "hh:mm — dd.MM.yyyy")
                            font.family: "lucida sans unicode"
                            font.pointSize: 8
                            anchors.bottom: borderimagelab10.bottom
                            anchors.right: borderimagelab10.right
                            anchors.rightMargin: outmessage ? 6:6
                            anchors.bottomMargin: 5
                        }
                    }
                }
            }
        }

        Rectangle{
            id: rectanlesend
            color: "#3d3d3d"
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            width: parent.width
            height: parent.height*0.1

            RowLayout {
                anchors.bottom: parent.bottom
                width: parent.width

                Rectangle {
                    anchors.fill: parent
                    color: "#3d3d3d"
                }

                TextArea {
                    id: areawritelab10
                    Layout.fillWidth: true
                    Layout.margins: 5
                    font.family: "lucida sans unicode"
                    font.pointSize: 12
                    color: "white"
                    placeholderTextColor: "#acf2ac"
                    placeholderText: "Сообщение..."
                    wrapMode: TextArea.WrapAtWordBoundaryOrAnywhere
                    background:

                        Item {
                        width: parent.width
                        height: parent.height
                    }
                }

                RoundButton {
                    Material.background: "#000000"
                    text: "🖂"
                    onClicked: {
                        if(areawritelab10.text!=""){
                            modellab10.append({                 // добавляет новый элемент в конец списка
                                                  "outmessage": true,
                                                  "message": areawritelab10.text,
                                                  "date" : new Date().toLocaleString(Qt.locale("ru_RU"))
                                              });
                            websocket.sendTextMessage(areawritelab10.text);
                            areawritelab10.clear();
                        }
                    }
                }
            }
        }

        WebSocket{
            id: websocket
            active: true
            url: "ws://localhost:8765"
            onTextMessageReceived: {
                console.log("message: ", message);
                modellab10.append(
                            {
                                "outmessage" : false,
                                "message" : message,
                                "date" : new Date().toLocaleString(Qt.locale("ru_RU"))
                            });
            }
            onStatusChanged: {
                switch(status)
                {
                case WebSocket.Connecting:
                    console.log("Socket is connecting");
                    break;
                case WebSocket.Open:
                    console.log("Socket is open");
                    break;
                case WebSocket.Closing:
                    console.log("Socket is closing");
                    break;
                case WebSocket.Closed:
                    console.log("Socket is closed");
                    break;
                case WebSocket.Error:
                    console.log("Socket is error");
                    console.log("Error = ", errorString);
                    break;
                }
            }
        }


}
