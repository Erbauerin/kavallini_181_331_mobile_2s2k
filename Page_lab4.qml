import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12 // без него не будет работать механизм layout
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.12 // для 2 лабы: воспроизведение и запись видео
import QtQuick.Dialogs 1.2 // для "открыть видео" во 2 лабе, пока не реализовано
import QtQuick.Controls.Styles 1.4 // можно менять стили кнопки, но в итоге пока не используется
import QtGraphicalEffects 1.0 // графические эффекты для 3 лабы




Page{  // лабораторная 4: HTTP-запросы
    id: page04
    Connections{
        target: Http_controller // объект - источник сигнал
        function onSignalSendToQML(pString, miniReply)
        {
            textarea.append(pString);
            textField.text = miniReply;
        }
    }

    header: ToolBar{
    id: header_lab4
    Material.background: "#000000"

    Rectangle {
        id: screen4
        height: 30
        width: parent.width
        property int pixelSize: screen4.height
        property color textColor: "#d9d9d9" //светло-серый

        color: "#000000"
        border.color: "#282828"
        Row {
            width: 300
            height: 10

            Text {
                id: text5;
                width: screen4.width
                horizontalAlignment: Text.AlignHCenter

                font.pixelSize: 17;
                font.family: "lucida sans unicode";

                color: screen4.textColor;
                text: "HTTP-запросы"
                anchors.top: parent.top
            }
        }
    }
}
    GridLayout {
            id: siteWeather


            anchors.fill: parent
            columns: 1
            rows: 3

            Flickable {
                id: flickable

                Layout.fillHeight: true
                Layout.fillWidth: true
                TextArea.flickable: TextArea {
                    id: textarea

                    textFormat: Text.PlainText
                    // Text.RichText - для вывода как в веб-версии
                    wrapMode: TextArea.Wrap

                    background: Rectangle {
                        id: rectangle
                        anchors.fill: parent
                        color: "#000"
                    }

                    readOnly: true



//                    BusyIndicator {
//                        id: busyIndicator
//                        anchors.centerIn: parent
//                        running: false

//                    }

                }

                ScrollBar.vertical: ScrollBar { }
            }

            Button {
                Layout.alignment: Qt.AlignHCenter
                text: "Получить данные"
                font.pixelSize: 12
                Material.background: "#4cc382"
                Material.foreground: "#000"
                onClicked: {
                    textarea.clear();
                    rectangle.color = "#86e39c" // фон во время загрузки данных
                    textField.text = "загрузка..."

                    signalMakeRequestHTTP();
                    rectangle.color = "#000" // фон после парсинга

                }
            }

            TextField {
                id: textField
                Layout.alignment: Qt.AlignHCenter
                horizontalAlignment: TextInput.AlignHCenter
                color: "#16bf62"
                font.pixelSize: 17

                readOnly: true
            }

        }


    }

/*Page{ // макет 4й лабы
    id: page_lab4
    GridLayout {
        anchors.fill:parent //привязываем по всем фронтам, для реализации таблицы
        rowSpacing: 2
        rows: 6
        columns: 2
        anchors.leftMargin: 20
        anchors.rightMargin: 20
        
        Button{
            id: btn
            onClicked: {
                signalMakeRequest();
            }
            text: "Отправить"
            Layout.row: 1
            Layout.column: 1
            
        }
        
        TextArea{
            id: textArea
            text: "..."
            
        }
        
        TextField{
            text: "whatis"
            readOnly: true
            Layout.row: 1
            Layout.column: 0
            selectedTextColor: "#000000"
            selectionColor: "#4cc382"
            color: "#16bf62"
            selectByMouse: true
        }
    }
}*/
