#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "http_controller.h"
#include "encription_controller.h"
#include "model_yandex.h"
#include <QQmlContext>

int main(int argc, char *argv[])
{       //вызов независимой функции в составе класса Коре - просто настройка масштабирования экрана, без создания класса
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
        //гуи(графику добавляет) наследуется от коре(самый базовый класс, поддержка ку-обджект, обработка командой строки)
    QGuiApplication app(argc, argv); //создается базовое приложение с графической областью
        //а кмл(самый сложный, навороченный, но нам не нужен) наследуется от гуи
    Http_controller Http_controller;
    Model_Yandex filemodel;
    Encriptioncontroller cipher_controller;
    QQmlApplicationEngine engine; //создание движка браузерного (!самое важное!) хромиум (рендеринг/визуализация написанного
      /* ИЗНАЧАЛЬНАЯ ВЕРСИЯ, которая была у меня
       где брать стартовую страницу для движка, преобразование пути стартовой страницы из Qchar в Qurl
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1; */
    QQmlContext * context = engine.rootContext(); //дерево объектов в QML движке
    context->setContextProperty("Http_controller", &Http_controller);
    context -> setContextProperty("fileModel", &Http_controller.fileModel);
    context -> setContextProperty("cipher_controller", &cipher_controller);

    //где брать стартовую страницу для движка, преобразование пути стартовой страницы из Qchar в QURL
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
    &app, [url](QObject *obj, const QUrl &objUrl) {
       //тело лямбда-выражения, вместо отдельного слота
        if (!obj && url == objUrl)
         QCoreApplication::exit(-1); //обработчик ошибок; подключение слота, срабатывающего по сигналу objectCreated
    }, Qt::QueuedConnection);
    engine.load(url); // загрузка стартовой страницы с адресом URL
    QObject * mainWindow = engine.rootObjects().first();
    QObject::connect(mainWindow, SIGNAL(signalMakeRequestHTTP()), &Http_controller, SLOT(GetNetworkValue()));
    return app.exec(); // запуск приложения и запуск(ВХОД) бесконечного цикла обработки сообщений и слотов сигналов
}

