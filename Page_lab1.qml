import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12 // без него не будет работать механизм layout
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.12 // для 2 лабы: воспроизведение и запись видео
import QtQuick.Dialogs 1.2 // для "открыть видео" во 2 лабе, пока не реализовано
import QtQuick.Controls.Styles 1.4 // можно менять стили кнопки, но в итоге пока не используется
import QtGraphicalEffects 1.0 // графические эффекты для 3 лабы

Page { // Лабараторная 1
    id: page_lab1
    // Оригинальный дизайн: https://c.radikal.ru/c35/2003/06/fbdc4111b5cb.jpg
    header: ToolBar{ // заголовок
        
        id: header_lab1
        Material.background: "#000000"
        
        Rectangle { // бегущая строка
            height: parent.height
            id: screen
            property int pixelSize: screen.height * 1.1
            property color textColor: "#d9d9d9" //светло-серый
            
            width: parent.width;
            //height: 40
            color: "#000000"
            border.color: "#282828"
            Row {
                //y: -screen.height / 13.5
                width: 300
                NumberAnimation on x { from: 0; to: -text.width ; duration: 10000; loops: Animation.Infinite }
                Text {
                    id: text;
                    font.pixelSize: 20;
                    font.family: "lucida sans unicode";
                    anchors.left: parent.left;
                    anchors.leftMargin: 50
                    color: screen.textColor;
                    text: qsTr("      Основы разработки приложений Qt QML. Элементы графического интерфейса.");
                    anchors.top: parent.top
                    anchors.topMargin: 10
                    anchors.horizontalCenter: screen.horizontalCenter
                    
                }
            }
        }
        Image{ // иконка в заголовке
            id: icon_bird
            source: "bird.JPG"
            width: 35
            height: 40
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            //  anchors.leftMargin: 5
        }
        
        Image{ // правая картинка в менюшке - звездочки
            id: decor
            source: "zvr.JPG"
            width: 20
            height: 25
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            //  anchors.leftMargin: 5
        }
    } // конец заголовка
    
    GridLayout {
        anchors.fill:parent //привязываем по всем фронтам, для реализации таблицы
        rowSpacing: 2
        rows: 6
        columns: 2
        anchors.leftMargin: 20
        anchors.rightMargin: 20
        
        
        RangeSlider {
            id: slider
            from: 0
            to: 1
            first.value: 0.1
            second.value: 0.7
            Layout.row: 1
            Layout.column: 0
            Layout.maximumWidth: 100
            Layout.maximumHeight: 20
        }
        Text {
            text: qsTr("Пределы кол-ва уведомлений за день")
            color: "#16bf62"
            Layout.row: 1
            Layout.column: 1
            Layout.maximumHeight: 20
        }
        
        TextField{
            id: txt
            text: "Actual_login"
            placeholderText: "введите логин..."
            color: "#16bf62"
            Layout.row: 2
            Layout.column: 0
            readOnly: { // можно ли печатать
                if (swit.checked == false){
                    return false
                }
                else
                    return true
            }
            Layout.maximumWidth: 100
            maximumLength: 20
            selectByMouse: { // можно ли выделять текст мышкой
                if (swit.checked == false){
                    return true
                }
                else
                    return false
            }
            selectedTextColor: "#000000"
            selectionColor: "#4cc382"
            
            /* echoMode: TextInput.PasswordEchoOnEdit - изначально точки/звёздочки, как только печатаются символы - они становятся видны
                            TextInput.Password - символы-звёздочки
                            TextInput.NoEcho - ничего не показывается
                  readOnly: true - запрет на печать
                */
        }
        Dial{
            
            //  snapMode: Dial.SnapOnRelease
            // wrap: true // - чтобы перетаскивать курсор напрямую с конечного до начального значения
            // position: 20
            Layout.maximumWidth: 50
            Layout.maximumHeight: 50
            Layout.row: 4
            Layout.column: 0
        }
        Text {
            text: qsTr("Громкость уведомлений")
            color: "#16bf62"
            Layout.row: 4
            Layout.column: 1
            
        }
        
        ProgressBar{
            value: slider.second.value
            //indeterminate: true
            Layout.maximumWidth: 100
            Layout.row: 3
            Layout.column: 0
            Layout.maximumHeight: 20
            
        }
        Text {
            text: qsTr("Нагрузка от объёма уведомлений")
            color: "#16bf62"
            Layout.row: 3
            Layout.column: 1
            Layout.maximumHeight: 20
        }
        
        Switch{
            id: swit
            text: {
                if (swit.checked == false)
                    return qsTr("Сохранить")
                else
                    return qsTr("Изменить")
            }
            //checked: true
            Layout.row: 2
            Layout.column: 1
            Layout.maximumHeight: 20
        }
        
        BusyIndicator{
            running: true
            Layout.row: 5
            Layout.column: 0
            // Layout.maximumHeight: 20
            Layout.rowSpan: 2
        }
        Text {
            text: qsTr("Ваш аватар")
            color: "#16bf62"
            Layout.row: 5
            Layout.column: 1
            Layout.rowSpan: 2
        }
    }
    
    
}
