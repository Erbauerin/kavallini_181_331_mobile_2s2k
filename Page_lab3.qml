import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12 // без него не будет работать механизм layout
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.12 // для 2 лабы: воспроизведение и запись видео
import QtQuick.Dialogs 1.2 // для "открыть видео" во 2 лабе, пока не реализовано
import QtQuick.Controls.Styles 1.4 // можно менять стили кнопки, но в итоге пока не используется
import QtGraphicalEffects 1.0 // графические эффекты для 3 лабы

Page{ // лабораторная 3: Графические эффекты
    id: page_lab3

    header: ToolBar{
        
        id: header_lab3
        Material.background: "#000000"
        
        Rectangle {
            id: screen3
            height: 30
            width: parent.width
            property int pixelSize: screen3.height
            property color textColor: "#d9d9d9" //светло-серый
            
            
            //height: 40
            color: "#000000"
            border.color: "#282828"
            Row {
                width: 300
                height: 10
                
                Text {
                    id: text3;
                    width: screen3.width
                    horizontalAlignment: Text.AlignHCenter

                    font.pixelSize: 17;
                    font.family: "lucida sans unicode";

                    color: screen3.textColor;
                    text: "Графические эффекты "
                    anchors.top: parent.top
                   // anchors.verticalCenter: screen3.width.horisontalCenter
                }
            }
        }
    }
    ColumnLayout {
        spacing: -5
        anchors.fill: parent //привязываем по всем фронтам
        anchors.leftMargin: 5
        anchors.bottomMargin: 5
        Layout.alignment: Qt.AlignHCenter

        RowLayout{
            id: cent_wind
            Layout.fillWidth: true


          Image {
              //  Layout.fillWidth: true
                Layout.preferredHeight: 300
                width: parent.width
                horizontalAlignment: Image.AlignHCenter
                id: img_filt
                
                source: {
                    if (change_img.checked == false)
                        return "ant.jpg"
                    else
                        return "ex.png"
                }
                fillMode: Image.PreserveAspectFit // изображение масштабируется равномерно
                visible: false
            }
          HueSaturation{
                id: first_effect
                source: img_filt
                anchors.fill: img_filt
                hue: effect_HueSaturation_hue.value // оттенок
                saturation: effect_HueSaturation_saturation.value // насыщенность
                lightness: effect_HueSaturation_lightness.value // яркость

            }
           ZoomBlur{
                id: second_effect
                source: first_effect
                anchors.fill: img_filt
                verticalOffset: effect_ZoomBlur_verticalOffset.value
                horizontalOffset: effect_ZoomBlur_horizontalOffset.value
                samples: effect_ZoomBlur_samples.value
                length: effect_ZoomBlur_length.value

            }
            DirectionalBlur{
                source: second_effect
                anchors.fill: img_filt
                angle: effect_DirectionalBlur_angle.value
                samples: effect_DirectionalBlur_samples.value
                length: effect_DirectionalBlur_length.value

            }
        }
        
        RowLayout{
         Layout.fillWidth: true
         Layout.alignment: Qt.AlignHCenter
            RadioButton {

                id: eff_hueSat
                checked: true
                text: qsTr("HueSaturation")
                font.pixelSize: 10
            }
            RadioButton {
                id: eff_zoom
                text: qsTr("ZoomBlur")
                font.pixelSize: 10
            }
            RadioButton {
                id: eff_direct
                text: qsTr("DirectionalBlur")
                font.pixelSize: 10
            }
        }
        

        RowLayout{
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter

            //############ HueSaturation ################
            Item{
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: eff_hueSat.bottom
                anchors.bottom: parent.bottom
                anchors.verticalCenter: parent.verticalCenter
                ColumnLayout{

                    spacing: -5
                    width: parent.width
                    RowLayout{
                        anchors.left: parent.left
                        anchors.right: parent.right
                            Text {
                                id: t_hue
                                text: qsTr("Hue")
                                color: "#16bf62"
                            }

                            Slider{
                                Layout.fillWidth: true
                                id: effect_HueSaturation_hue
                                from: -1.0
                                to: 1.0
                                stepSize: 0.05
                                value: 0.0
                            }

                    }
                    RowLayout{
                        anchors.left: parent.left
                        anchors.right: parent.right

                            Text {
                                text: qsTr("Lightness")
                                color: "#16bf62"
                            }

                            Slider{
                                 Layout.fillWidth: true
                                id: effect_HueSaturation_lightness
                                from: -1.0
                                to: 1.0
                                stepSize: 0.1
                                value: 0.0
                                
                            }

                    }
                    RowLayout{
                        anchors.left: parent.left
                        anchors.right: parent.right
                            Text {
                                text: qsTr("Saturation")
                                color: "#16bf62"
                            }

                            Slider{
                                 Layout.fillWidth: true
                                id: effect_HueSaturation_saturation
                                from: -1.0
                                to: 1.0
                                stepSize: 0.1
                                value: 0.0
                            }
                    }
                    RowLayout{
                        Switch{ // смена картинки
                            id: change_img
                            text: qsTr("Сменить картинку")
                            checked: false
                            font.pixelSize: 10
                        }
                    }
                    visible: {
                        if(eff_hueSat.checked == true){
                            return true
                        }
                        else
                            return false
                    }
                }
            }
            //############ ZoomBlur ################
            
            Item{
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: eff_hueSat.bottom
                anchors.bottom: parent.bottom
                anchors.verticalCenter: parent.verticalCenter

                ColumnLayout{
                    spacing: -5
                    width: parent.width
                    RowLayout{

                        anchors.left: parent.left
                        anchors.right: parent.right
                            Text {
                                text: qsTr("verticalOffset")
                                color: "#16bf62"
                            }

                            Slider{
                                Layout.fillWidth: true
                                id: effect_ZoomBlur_verticalOffset
                                from: -50.0
                                to: 50.0
                                stepSize: 10.0
                                value: 0.0
                            }

                    }
                    RowLayout{
                        anchors.left: parent.left
                        anchors.right: parent.right
                            Text {
                                text: qsTr("horizontalOffset")
                                color: "#16bf62"
                            }

                            Slider{
                                 Layout.fillWidth: true
                                id: effect_ZoomBlur_horizontalOffset
                                from: -50.0
                                to: 50.0
                                stepSize: 10.0
                                value: 0.0
                            }
                    }
                    RowLayout{
                        anchors.left: parent.left
                        anchors.right: parent.right
                            Text {
                                text: qsTr("samples")
                                color: "#16bf62"
                            }

                            Slider{
                                 Layout.fillWidth: true
                                id: effect_ZoomBlur_samples
                                from: 0.0
                                to: 80.0
                                stepSize: 10.0
                                value: 0.0
                            }

                    }
                    RowLayout{
                        anchors.left: parent.left
                        anchors.right: parent.right
                            Text {
                                text: qsTr("length")
                                color: "#16bf62"
                            }

                            Slider{
                                 Layout.fillWidth: true
                                id: effect_ZoomBlur_length
                                from: 0.0
                                to: 80.0
                                stepSize: 10.0
                                value: 0.0
                                //anchors.bottomMargin: 10
                            }

                    }
                    
                }
                
                visible: {
                    if(eff_zoom.checked == true){
                        return true
                    }
                    else
                        return false
                }
            }
            //############ DirectionalBlur ################
            
            Item{
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: eff_hueSat.bottom
                anchors.bottom: parent.bottom
                anchors.verticalCenter: parent.verticalCenter

                ColumnLayout{
                    spacing: -5
                    width: parent.width
                    RowLayout{
                        anchors.left: parent.left
                        anchors.right: parent.right
                            Text {
                                text: qsTr("angle")
                                color: "#16bf62"
                            }

                            Slider{
                                 Layout.fillWidth: true
                                id: effect_DirectionalBlur_angle
                                from: -180.0
                                to: 180.0
                                stepSize: 20.0
                                value: 0.0
                            }

                    }
                    RowLayout{
                        anchors.left: parent.left
                        anchors.right: parent.right
                            Text {
                                text: qsTr("samples")
                                color: "#16bf62"
                            }

                            Slider{
                                 Layout.fillWidth: true
                                id: effect_DirectionalBlur_samples
                                from: 0.0
                                to: 10.0
                                stepSize: 0.1
                                value: 0.0
                            }

                    }
                    RowLayout{
                        anchors.left: parent.left
                        anchors.right: parent.right
                            Text {
                                text: qsTr("length")
                                color: "#16bf62"
                            }

                            Slider{
                                 Layout.fillWidth: true
                                id: effect_DirectionalBlur_length
                                from: 0.0
                                to: 10.0
                                stepSize: 0.1
                                value: 0.0
                            }

                    }
                    visible: {
                        if(eff_direct.checked == true){
                            return true
                        }
                        else
                            return false
                    }
                }
            }
        } // общая строка итемов
        
    }

}
