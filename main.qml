import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12 // без него не будет работать механизм layout
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.12 // для 2 лабы: воспроизведение и запись видео
import QtQuick.Dialogs 1.2 // для "открыть видео" во 2 лабе, пока не реализовано
import QtQuick.Controls.Styles 1.4 // можно менять стили кнопки, но в итоге пока не используется
import QtGraphicalEffects 1.0 // графические эффекты для 3 лабы
import QtQuick.Window 2.12
import QtQml 2.12
import QtWebView 1.1
import QtWebSockets 1.1


ApplicationWindow {
    id: general_Window // идентификатор окна(?) должен начинаться с МАЛЕНЬКОЙ буквы!!!
    // если к объекту не планируется обращаться, то id можно не задавать
    signal signalMakeRequestHTTP();
    visible: true
    width: 320 // ширина окна
    height: 480 // высота окна
    title: qsTr("Главное окно") // имя окошка, только в кавычках можно писать по-русски
    //qsTr - функция для переключения языка строки (если два языка, то нужно предоставить две строки)
    Material.accent: "#16bf62" //насыщенно-зелёный
    Material.background: "#000000" // чёрный
    property string token1: ""

SwipeView {
    id: swipeView
    anchors.fill: parent
    currentIndex: tabBar.currentIndex

    Page_lab1 {
        id: page_lab1
    }

    Page_lab2 {
        id: page_lab2
    }



    Page_lab3 {
        id: page_lab3
    }



    Page_lab4 {
        id: page_lab4
    }



    Page_lab5 {
        id: page_lab5
    }

    Page_lab6 {
        id: page_lab6
    }

    Page_lab7 {
        id: page_lab7
    }

    Page_lab10 {
        id: page_lab10
    }

}
    Drawer{
        id: drawer
        width: 0.45 * parent.width
        height: parent.height

        GridLayout{
            width: parent.width
            columns: 1

            Button{
                text: "Лабараторная 1"
                flat: true
                onClicked: {
                    swipeView.currentIndex = 0
                    drawer.close()
                }
            }

            Button{
                text: "Лабараторная 2"
                flat: true
                onClicked: {
                    swipeView.currentIndex = 1
                    drawer.close()
                }
            }
            Button{
                text: "Лабараторная 3"
                flat: true
                onClicked: {
                    swipeView.currentIndex = 2
                    drawer.close()
                }
            }
            Button{
                text: "Лабараторная 4"
                flat: true
                onClicked: {
                    swipeView.currentIndex = 3
                    drawer.close()
                }
            }
            Button{
                text: "Лабараторная 5"
                flat: true
                onClicked: {
                    swipeView.currentIndex = 4
                    drawer.close()
                }
            }
            Button{
                text: "Лабараторная 6"
                flat: true
                onClicked: {
                    swipeView.currentIndex = 5
                    drawer.close()
                }
            }
            Button{
                text: "Лабараторная 7"
                flat: true
                onClicked: {
                    swipeView.currentIndex = 6
                    drawer.close()
                }
            }
            Button{
                text: "Лабараторная 10"
                flat: true
                onClicked: {
                    swipeView.currentIndex = 7
                    drawer.close()
                }
            }
//            Button{
//                text: "Лабараторная 9"
//                flat: true
//                onClicked: {
//                    swipeView.currentIndex = 8
//                    drawer.close()
//                }
//            }
//            Button{
//                text: "Лабараторная 10"
//                flat: true
//                onClicked: {
//                    swipeView.currentIndex = 9
//                    drawer.close()
//                }
//            }

        }
    }



/* // вкладочки снизу страницы
footer: TabBar {
    id: tabBar
    currentIndex: swipeView.currentIndex

    TabButton {
        text: qsTr("Lab 1")
    }
    TabButton {
        text: qsTr("Lab 2")
    }
    TabButton {
        text: qsTr("Lab 3")
    }

  }*/

    /*  Page { //страница демонстрации layout

          GridLayout {
              anchors.fill:parent //привязываем по всем фронтам, для реализации таблицы
              columns:  2
              Button {
                  text: "Button"
                  width: 100
                  height: 100
                  font.pixelSize: 30
              }

              Button {
                  text: "Button"
                  width: 100
                  height: 100
                  font.pixelSize: 30
              }

              Button {
                  text: "Button"
                  width: 100
                  height: 100
                  font.pixelSize: 30
              }

              Button {
                  text: "Button"
                  width: 100
                  height: 100
                  font.pixelSize: 30
              }
              Button {
                  text: "tes5"
                  width: 100
                  height: 100
                  font.pixelSize: 30
                  Layout.row: 3
                  Layout.column: 1
                  Layout.preferredHeight: 200 // настраивает предпочтительную высоту
                  Layout.preferredWidth: 100 // настраивает предпочтительную ширину
              }
          }
      }

      Page{ //страница демонстрации anchor
          Text {
              x: 1
              y: 10
              id: name
              text: qsTr("Текст перед кнопкой")
              color: "yellow"
              // вариант фонта 1
              font{
                  pixelSize: 13
                  family: "Times New Roman"
                  bold: true
              }
          }

          Button {
              id: but1
              y: 100
              text: "Кнопочка1"
              // вариант фонта 2
              font.family: "Arial"
              font.bold: true
              font.pixelSize: 20
          }

          Button {
              text: "Button"
              y: 100
              anchors.right: parent.right
              anchors.left: button1.right
              anchors.margins: 10 // отступ относительно предыдущего anchors (круговой - по всем краям)
              anchors.top: parent.top // привязка к верхней части окна
              font.pixelSize: 30
          }

          Button {
              text: "Button"
              y: 100
              anchors.centerIn: parent
              font.pixelSize: 30
          }
      }
  */

}

