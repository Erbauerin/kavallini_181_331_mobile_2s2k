#ifndef HTTP_CONTROLLER_H
#define HTTP_CONTROLLER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QJsonArray>
#include "model_yandex.h"

class Http_controller : public QObject
{
    Q_OBJECT
public:
    explicit Http_controller(QObject *parent = nullptr);
    QNetworkAccessManager *nam;
    Model_Yandex fileModel;


public slots:
     void GetNetworkValue();  // метод для запроса странички с сервера

     QString onPageInfo(QString replyString);

     QString auth(QString urlforauth);
     bool authbool(QString urlforauth);
     QByteArray requestReceivingAPI(QString token);


 signals:
     void signalSendToQML(QString pReply, QString miniReply);
     void signalSendToQML_2(QString currentratecostrub);
     void signalSendToQML_3(int sizeLess1, int sizeLess2, int sizeLess3, int sizeLess4, int sizeLess5, int sizeOver5);

};

#endif // HTTP_CONTROLLER_H









//  void database_read();
//  void database_write(QByteArray source);






