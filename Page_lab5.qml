import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12 // без него не будет работать механизм layout
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.12 // для 2 лабы: воспроизведение и запись видео
import QtQuick.Dialogs 1.2 // для "открыть видео" во 2 лабе, пока не реализовано
import QtQuick.Controls.Styles 1.4 // можно менять стили кнопки, но в итоге пока не используется
import QtGraphicalEffects 1.0 // графические эффекты для 3 лабы
import QtQuick 2.0
import QtQuick.Window 2.12
import QtQml 2.12
import QtWebView 1.1
import QtWebSockets 1.1

Page{ // 5 лабораторная: Аутентификация через Яндекс_Диск
    id: page_lab5
    Material.background: "#000000"

    header: ToolBar{
    id: header_lab5
    Material.background: "#000000"

    Rectangle {
        id: screen5
        height: 30
        width: parent.width
        property int pixelSize: screen5.height
        property color textColor: "#d9d9d9" //светло-серый

        color: "#000000"
        border.color: "#282828"
        Row {
            width: 300
            height: 10

            Text {
                id: text5;
                width: screen5.width
                horizontalAlignment: Text.AlignHCenter

                font.pixelSize: 17;
                font.family: "lucida sans unicode";

                color: screen5.textColor;
                text: "Аутентификация"
                anchors.top: parent.top
            }
        }
    }
}

        WebView {
            id: browserlab5
            anchors.fill: parent

            onLoadingChanged: {
                var token = Http_controller.auth(browserlab5.url)
                var tokenbool = Http_controller.authbool(browserlab5.url)

                browserlab5.visible = token === " " ? true : false;
                btngettoken.visible = token === " " ? false : true;
                labelfortoken.text = token
                token1 = token
                labelfortoken.color = tokenbool === true ? "#1f7834" : "#a11630"
                labelsucsesslab5.visible = tokenbool === true ? true : false
                //console.info(token + " token    " + tokenbool)
                var gfdfgdfgf = Http_controller.requestReceivingAPI(token)
                //labelfortoken.text = gfdfgdfgf
            }
        }

        Label {
            id: labelfortoken
            anchors.bottom: btngettoken.top
            anchors.bottomMargin: 170
            anchors.horizontalCenter: parent.horizontalCenter
            font.family: "SF UI Display"
            font.bold: {
                if (token1 == "Не удалось получить токен")
                    return true
                else
                    return false
            }
            font.pixelSize: {
                if (token1 == "Не удалось получить токен")
                    return 23
                else
                    return 14
                }
        }

        Label {
            id: labelsucsesslab5
            anchors.bottom: btngettoken.top
            anchors.bottomMargin: 190
            visible: false
            anchors.horizontalCenter: parent.horizontalCenter
            font.family: "SF UI Display"
            font.pixelSize: 21
            text: "Токен успешно получен!"
            color: "#77BD8B"
        }

        Label {
            id: labelnosucsesslab5
            visible: true
            color: "#000000"
            anchors.bottom: btngettoken.top
            anchors.bottomMargin: 150
            anchors.horizontalCenter: parent.horizontalCenter
            font.family: "SF UI Display"
            font.pixelSize: 19
            text: "Требуется войти в аккаунт \nдля получения доступа к фото \nс Вашего Яндекс.Диска."
        }

        Button {
            id:btngettoken
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Получить токен"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 130
            font.family: "SF UI Display Light"
            font.pixelSize: 21


            onClicked: {
                browserlab5.url = "https://oauth.yandex.ru/authorize?response_type=token"
                        +"&client_id=2339bbbee09c49d5a2a1bdb6b9804bf8"
                        +"&device_id=123456546546"
                        +"&device_name=phone"
                        +"&redirect_uri=https://oauth.yandex.ru/verification_code"
                        +"&force_confirm=yes"
                        +"&state=get_token"
                        +"&display=popup",
                labelnosucsesslab5.visible = false
            }
        }


}
