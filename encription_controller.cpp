#include "encription_controller.h"
#include <QString>
#include "openssl/evp.h" // не видит путь, когда в угловых скобках
#include <QFile>
#include <QByteArray>
#include <QIODevice>
#include <QObject>
#include <QTemporaryFile>
#include <openssl/conf.h>
#include <openssl/err.h>
#include <openssl/aes.h>
#include <QBuffer>

Encriptioncontroller::Encriptioncontroller(QObject *parent):QObject (parent)
{

}

bool Encriptioncontroller::encriptFile(const QString & mkey, const QString & in_file){ //, const QString & out_file

    EVP_CIPHER_CTX *ctx; // СИшная структура
    if(!(ctx = EVP_CIPHER_CTX_new())){ // выделение памяти, аналогичное оператору-C++ new()
        return false;
    }
  //  unsigned char * key[256] = {0}; - сюда записываем содержимое строки mkey
    //mkey.toLatin1().data(); - массив символов, из которых состояла строка
   // memcpy_s(key, 256, mkey.toLatin1().data(), 256); - если не заработает reinterpret_cast

    if(1 != EVP_EncryptInit_ex(ctx, // уже выделенная в памяти структура, в которую будут заноситься параметры
                               EVP_aes_256_cfb(), // параметр, отвечающий за тип шифрования
                               NULL,
                               reinterpret_cast<unsigned char *>(mkey.toLatin1().data()), // ключ шифрования, из QML
                               m_iv)) // вектор инициализации
    {
        return false;
    }

    unsigned char ciphertext[256] = {0};
    unsigned char plaintexttext[256] = {0};
    int len = 0, plaintext_len = 0;
    // открыть файл с исходными данными (только для чтения) через QFile file_in(mkey); file_in.open(QFile::ReadOnly);
    soursefile = in_file.mid(8);
    QFile sourse_file(soursefile);
    sourse_file.open(QIODevice::ReadOnly);

    int position = soursefile.lastIndexOf(".");
    QString file_extension = soursefile.mid(position);
    QString soursefile_enc = soursefile.left(position) + "_enc" + file_extension;

    //и создать (открыть с перезаписью) файл с шифрованными данными
    QFile file_modificate(soursefile_enc);
    file_modificate.open(QIODevice::ReadWrite | QIODevice::Truncate);
    plaintext_len = sourse_file.read((char *)plaintexttext, 256);

    while(plaintext_len > 0/*условие конфа файла*/){ // цикл шифрования

        // 1. Считать очередную порцию данных из файла в буфер plaintext
        //...
        // 2.Применить функцию EVP_EncryptUpdate для получения ciphertext и len
        if(1 != EVP_EncryptUpdate(ctx, // уже заполненная структура с настройками
                                  ciphertext, // выходной параметр, бувур, куда записывается шифрованный текст
                                  &len, // выходной параметр, количество зашифрованных символов, объем в байтах
                                  plaintexttext, // входной параметр, шифруемый буфер
                                  plaintext_len)) // входной параметр, количество исходных символов
        {
            return false;
        }

        //Запись ciphertext в файл шифрованных данных
        file_modificate.write((char *)ciphertext, len);
        plaintext_len = sourse_file.read((char *)plaintexttext, 256);

    }

    if(1 != EVP_EncryptFinal_ex(ctx,
                                ciphertext + len,
                                &len))
    {
        return false;
    }
    file_modificate.write((char*)ciphertext, len);
    EVP_CIPHER_CTX_free(ctx);
      // закрытие файлов
    sourse_file.close();
    file_modificate.close();

    return true;
}

bool Encriptioncontroller::decriptFile(const QString & mkey, const QString & in_file){ //, const QString & out_file

    EVP_CIPHER_CTX *ctx; // СИшная структура
    if(!(ctx = EVP_CIPHER_CTX_new())){ // выделение памяти, аналогичное оператору-C++ new()
        return false;
    }
    if(1 != EVP_DecryptInit_ex(ctx, // уже выделенная в памяти структура, в которую будут заноситься параметры
                               EVP_aes_256_cfb(), // параметр, отвечающий за тип шифрования
                               NULL,
                               reinterpret_cast<unsigned char *>(mkey.toLatin1().data()), // ключ шифрования, из QML
                               m_iv)) // вектор инициализации
    {
        return false;
    }
    unsigned char ciphertext[256] = {0};
    unsigned char plaintexttext[256] = {0};
    int len = 0, plaintext_len = 0;
    // открыть файл с исходными данными (только для чтения) через QFile file_in(mkey); file_in.open(QFile::ReadOnly);
    soursefile = in_file.mid(8);
    QFile sourse_file(soursefile);
    sourse_file.open(QIODevice::ReadOnly);

    int position = soursefile.lastIndexOf(".");
    QString file_extension = soursefile.mid(position);
    QString soursefile_dec = soursefile.left(position) + "_dec" + file_extension;

    //и создать (открыть с перезаписью) файл с шифрованными данными
    QFile file_modificate(soursefile_dec);
    file_modificate.open(QIODevice::ReadWrite | QIODevice::Truncate);
    plaintext_len = sourse_file.read((char *)plaintexttext, 256);

    while(plaintext_len > 0/*условие конфа файла*/){ // цикл шифрования

        // 1. Считать очередную порцию данных из файла в буфер plaintext
        //...
        // 2.Применить функцию EVP_EncryptUpdate для получения ciphertext и len
        if(1 != EVP_DecryptUpdate(ctx, // уже заполненная структура с настройками
                                  ciphertext, // выходной параметр, бувур, куда записывается шифрованный текст
                                  &len, // выходной параметр, количество зашифрованных символов, объем в байтах
                                  plaintexttext, // входной параметр, шифруемый буфер
                                  plaintext_len)) // входной параметр, количество исходных символов
        {
            return false;
        }
        //Запись ciphertext в файл шифрованных данных
        file_modificate.write((char *)ciphertext, len);
        plaintext_len = sourse_file.read((char *)plaintexttext, 256);

    }
    if(!EVP_DecryptFinal_ex(ctx, ciphertext + len, &len))
                return false;

    file_modificate.write((char*)ciphertext, len);
    EVP_CIPHER_CTX_free(ctx);
      // закрытие файлов
    sourse_file.close();
    file_modificate.close();

    return true;
}
