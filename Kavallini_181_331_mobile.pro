#список, подключенных разделов библиотеки Qt - для работы с сетью - дописываем network, чтоб с nfs работало - дописываем его.
QT += quick network
QT += sql # для работы sqldatabase
QT += core # для работы eventloop
#ключевые слова, настройки компиляции - пользоваться не будем, хватает этого
CONFIG += c++11
#QT += charts
QT += qml

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.

#объявление переменных и флагов окружения
DEFINES += QT_DEPRECATED_WARNINGS #супер-осторожный режим, выводит все предупреждения, даже устаревшие

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
#DEPENDPATH += \
 #       C:\Qt\Tools\OpenSSL\Win_x64\include\openssl #хотя вроде можно и без последней папки
# TEMPNAME=$${QMAKE_QMAKE} - полный путь до экзешника qmake.exe
#$$QTPATH = $$dirname(TEMPNAME)\..\..\.. - здесь возвращаемся вверх на 3 уровня, его ($$QTPATH) можно использовать вместо абсолютной части пути (C:\Qt)
INCLUDEPATH += \
        C:\Qt\Tools\OpenSSL\Win_x64\include\openssl # загаловочные файлы
LIBS += \
        C:\Qt\Tools\OpenSSL\Win_x64\lib\libcrypto.lib # библиотека, содержащая таблицу соответствий названий функций и их адресов
INCLUDEPATH += C:/Qt/Tools/OpenSSL/Win_x64/include
LIBS += -L"C:/Qt/Tools/OpenSSL"
#раздел файлов исходного кода на С++
SOURCES += \
        main.cpp \
    encription_controller.cpp \
    http_controller.cpp \
    model_yandex.cpp

#HEADERS - раздел файлов заголовков С++

#список файлов, включаемых в раздел ресурсов, получаемого исполняемого модуля (исполняемый файл/программка)
#то есть текстовые, мультимедиа файлы
RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model - пока не нужно
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer - пока не нужно
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
#название платформы - для чего?
#cинтаксис: "название платформы:" - последующие команды сборки будут работать только на обозначенной платформе
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    encription_controller.h \
    http_controller.h \
    model_yandex.h


QMAKE_EXTRA_TARGETS += before_build makefilehook
makefilehook.target = $(MAKEFILE)
makefilehook.depends = .beforebuild

PRE_TARGETDEPS += .beforebuild
before_build.target = .beforebuild
before_build.depends = FORCE
before_build.commands = chcp 1251
