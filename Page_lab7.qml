import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12 // без него не будет работать механизм layout
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.12 // для 2 лабы: воспроизведение и запись видео
import QtQuick.Dialogs 1.2 // для "открыть видео" во 2 лабе, пока не реализовано
import QtQuick.Controls.Styles 1.4 // можно менять стили кнопки, но в итоге пока не используется
import QtGraphicalEffects 1.0 // графические эффекты для 3 лабы
import QtQuick.Window 2.12
import QtQml 2.12
import QtWebView 1.1
import QtWebSockets 1.1


Page{ // лабараторная 7: OpenSSL
    id: page_lab7

    header: ToolBar{

        id: header_lab7
        Material.background: "#000000"

        Rectangle {
            id: screen7
            height: 30
            width: parent.width
            property int pixelSize: screen7.height
            property color textColor: "#d9d9d9" //светло-серый

            color: "#000000"
            border.color: "#282828"
            Row {
                width: 300
                height: 10

                Text {
                    width: screen7.width
                    horizontalAlignment: Text.AlignHCenter

                    font.pixelSize: 17;
                    font.family: "lucida sans unicode";

                    color: screen7.textColor;
                    text: "OpenSSL"
                    anchors.top: parent.top
                }
            }
        }
    }

    Label{
            text: "Введите ключ шифрования\n            (32 символа)"
            anchors.horizontalCenter: parent.horizontalCenter
            font.family: "lucida sans unicode"
            font.pixelSize: 17
            font.bold: true
            anchors.bottom: textforkey.top
            anchors.bottomMargin: 60
        }

        TextField{
            id: textforkey
            placeholderText: qsTr("Введите ключ сюда")
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: chipheronlab7.top
            horizontalAlignment: TextInput.AlignHCenter
            width: 320
            font.family: "lucida sans unicode"
            font.pixelSize: 15
            anchors.bottomMargin: 40
            maximumLength: 32
        }

        Button{
            id: chipheronlab7
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            text: "Зашифровать"
            font.family: "lucida sans unicode"
            font.pixelSize: 18
            anchors.bottomMargin: 120
            Material.background: "#4cc382"
            Material.foreground: "#000"
            flat: true
            visible: if(fileDialoglab7.fileUrl === 0)
                         false
                     else
                         true
            onClicked: {cipher_controller.encriptFile(textforkey, fileDialoglab7.fileUrls)
                        if (cipher_controller.encriptFile(textforkey, fileDialoglab7.fileUrls) === true){
                         chipheronlab7.Material.background = "#a8f25e" // желтоватый
                         chipherofflab7.Material.background = "#4cc382" // зеленоватый
                        }
            }

        }

        Button{
            id: chipherofflab7
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: chipheronlab7.bottom
            anchors.topMargin: 30
            text: "Расшифровать"
            font.family: "lucida sans unicode"
            Material.background: "#4cc382"
            Material.foreground: "#000"
            font.pixelSize: 18
            flat: true
            visible: if(fileDialoglab7.fileUrl === 0)
                         false
                     else
                         true
            onClicked: {cipher_controller.decriptFile(textforkey, fileDialoglab7.fileUrls)
                        if (cipher_controller.decriptFile(textforkey, fileDialoglab7.fileUrls) === true){
                            chipheronlab7.Material.background = "#4cc382"
                            chipherofflab7.Material.background = "#a8f25e"
                        }

            }
        }

        Button {
            id: btnfordialoglab7
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 15
            anchors.top: header_lab7.bottom
            anchors.bottomMargin: 100
            anchors.leftMargin: 10
            flat: true
            text: "Выбрать файл"
            onClicked: fileDialoglab7.open()
            Material.background: "#4cc382"
            Material.foreground: "#000"

            FileDialog {
                id: fileDialoglab7
                folder: "C:\\Kavallini_181_331\\Kavallini_181_331_mobile\\"
            }
        }
}
